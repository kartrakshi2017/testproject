package Datacsv;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.opencsv.CSVReader;

public class CSVRead {

 //Provide CSV file path. It Is In D: Drive.
 String CSV_PATH="C:\\Users\\Probe7\\Desktop\\TestData.xlsx";
 WebDriver driver;

 
 @BeforeTest
 public void setup() throws Exception {
	 
  System.setProperty("webdriver.chrome.driver","D://Karthik//New soft//chromedriver_win32//chromedriver.exe");
  driver = new ChromeDriver();
  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  driver.manage().window().maximize();
  driver.get("https://test3.icoreemr.com");
 }
 
 @Test
 public void csvDataRead() throws IOException{
  
  CSVReader reader = new CSVReader(new FileReader(CSV_PATH));
  String [] csvCell;
  //while loop will be executed till the last line In CSV.
  while ((csvCell = reader.readNext()) != null) {  
	 String Username = csvCell[0];
	 String Password = csvCell[1];
//   String FName = csvCell[0];
//   String LName = csvCell[1];
//   String Email = csvCell[2];
//   String Mob = csvCell[3];
//   String company = csvCell[4];
   driver.findElement(By.cssSelector("#username")).sendKeys(Username);
   driver.findElement(By.cssSelector("#password")).sendKeys(Password);
   driver.findElement(By.cssSelector("#login")).click();
   driver.switchTo().alert().accept();
  }  
 }
}