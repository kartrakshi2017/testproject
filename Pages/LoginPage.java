package Pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import Utility.*;
//we can able to acceess all the functions inside the package using "*"

public class LoginPage {
	
	WebDriver driver;
	
	static String Path_TestData = "C:\\Users\\Probe7\\Desktop\\TestData.xlsx";
	
	//Login Properties:
	By username = By.id("username");
	By Password = By.id("password");
	By submit = By.id("login");
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;

	}
	
	public void Logins() throws IOException {
		
		FileInputStream file;
		try {
			file = new FileInputStream(Path_TestData);
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			for(Row row:sheet) {
				for(Cell cell:row) {
					System.out.print(cell.getStringCellValue()+"\t");
					
			driver.findElement(username).sendKeys(cell.getStringCellValue());
			driver.findElement(Password).sendKeys("Y9F!jk@bpPhJ4s");
			driver.findElement(submit).click();
			
				}
			}
					
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
}
}	
